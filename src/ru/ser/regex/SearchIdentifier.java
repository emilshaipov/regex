package ru.ser.regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Shaipov Emil'
 *
 */

public class SearchIdentifier {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile("\\b\\d+\\b");
        Matcher matcher = pattern.matcher("");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\regex\\id.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                }
            }
        }
    }
}
